package com.example.springboot;

import org.slf4j.MDC;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String index(@RequestHeader("accept") String acceptHeader) {
		MDC.put("acceptHeader", acceptHeader);
		LoggerFactory.getLogger(getClass()).info("Hello");
		return "Greetings from Spring Boot!";
	}

}
