# Spring Boot Example Applications with Structured Logging

A companion repo for [_Migrating Your Spring Boot Application to use Structured Logging_](https://www.jvt.me/posts/2021/05/31/spring-boot-structured-logging/)
